import {Routes} from '@angular/router'
import { AboutUsComponent } from './about-us/about-us.component'
import { ProductsComponent } from './products/products.component'
import { BoardAdminComponent } from './board-admin/board-admin.component'
import { ProfileComponent } from './profile/profile.component'
import { RegisterComponent } from './register/register.component'
import { LoginComponent } from './login/login.component'
import { HomeComponent } from './home/home.component'
import { ProductDescriptionComponent } from './config/product-description/product-description.component'
import { DemandeComponent } from './demande/demande.component'
import { ProductAdressComponent } from './products/product-demande/product-adress/product-adress.component'
import { ProductDemandeHeuresComponent } from './products/product-demande/product-demande-heures/product-demande-heures.component'
import { ProductDemandeTelephoneComponent } from './products/product-demande/product-demande-telephone/product-demande-telephone.component'
import { ProductDemandePhotoComponent } from './products/product-demande/product-demande-photo/product-demande-photo.component'

export const routes: Routes = [

  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'admin', component: BoardAdminComponent },
  { path: 'description', component: ProductDescriptionComponent },
  { path: 'products', component: ProductsComponent,
  children: [
    { path: 'adress', component: ProductAdressComponent },
    { path: 'heures', component: ProductDemandeHeuresComponent },
    { path: 'telephone', component: ProductDemandeTelephoneComponent },
    { path: 'photo', component: ProductDemandePhotoComponent },
    { path: '', redirectTo: 'products/adress', pathMatch: 'full' }
  ]
  },
  { path: 'aboutUs', component: AboutUsComponent },
  { path: 'demande', component: DemandeComponent }

  
]