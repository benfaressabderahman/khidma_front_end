import { Component, ElementRef, OnInit, ViewChild, inject } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { post } from '../store/products/products.model';
import { AddPost } from '../store/products/products.action';
import { Router } from '@angular/router';
import { ProductsService } from '../_services/products.service';
import { CartesComponent } from '../config/cartes/cartes.component';
import { CategoryService } from '../_services/category.service';
import { ProductDemandeComponent } from './product-demande/product-demande.component';
import { provideNativeDateAdapter } from '@angular/material/core';

@Component({
  selector: 'app-products',
  standalone:true,
  imports:[ReactiveFormsModule, ProductDemandeComponent],
  templateUrl: './products.component.html',
  styleUrl: './products.component.scss'
})
export class ProductsComponent implements OnInit{



  form= new FormGroup({
    productName: new FormControl(''),
    productPrice : new FormControl(),
    productDescription: new FormControl(''),
    selectedValueCategory: new FormControl(''),
    selectedValueSousCategory: new FormControl('')
  });

@ViewChild('dropdown', {static : false}) dropdown?:ElementRef;

  data : any;

  selectedFile1!:any;

  selectedFile2!:any;

  selectedValueCategory :any;
  selectedValueSousCategory : any;
  values: any[] = [];

  sousCategory : any;
  selectedValueCategoryt:any;


  //access the state and the slice needed
  //public posts$: Observable<post[]> = this.store.select(state => state.posts.listPosts);

  constructor(private route : Router, 
    private productsService : ProductsService,
    private categoryService : CategoryService
  ) { }

  ngOnInit(): void {
    this.categoryService.getCategories().subscribe({
      next: data =>{
        this.values = JSON.parse(data);
      }
    })

  }

  onSubmit() {

    if (this.form.valid) {

      const {productName, productPrice, productDescription, selectedValueCategory, selectedValueSousCategory} = this.form.value;

      var productImage1 = this.selectedFile1;
      var productImage2=this.selectedFile2;

      if(productName && productPrice && productDescription && productImage1 && productImage2 && selectedValueCategory && selectedValueSousCategory){
      const formData = new FormData();
      formData.append('idCategory', selectedValueCategory);
      formData.append('idSousCategory', selectedValueSousCategory);
      formData.append('productName', productName);
      formData.append('productPrice', productPrice);
      formData.append('productDescription', productDescription);
      formData.append('productImage1', productImage1);
      formData.append('productImage2', productImage2);

      this.productsService.postProducts(formData).subscribe((data)=>{
        this.data = data;
      });
      }


/*       this.store.dispatch(new AddPost(_post));
 */      
//this.route.navigate(["/home"]);
      // Here you can send the form data to your backend or perform any other actions
    } else {
    }
  }

  onFileSelected1(event:any){
    this.selectedFile1 = event.target.files[0];

  }

  onFileSelected2(event:any){
    this.selectedFile2 = event.target.files[0];

  }

  onSelect(e : Event){
    const selectElement = e.target as HTMLSelectElement;

    this.selectedValueCategoryt = selectElement.value;
    this.categoryService.getSousCategoryById(this.selectedValueCategoryt).subscribe({
      next: (data)=>{
        this.sousCategory = JSON.parse(data);
      }
    })

  }
}
