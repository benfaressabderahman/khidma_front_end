import { ChangeDetectionStrategy, Component, model} from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatCardModule} from '@angular/material/card';
import { MatNativeDateModule, provideNativeDateAdapter } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { ProductDemandeHeuresComponent } from './product-demande-heures/product-demande-heures.component';
import { ProductAdressComponent } from './product-adress/product-adress.component';
import { ActivatedRoute, Router, RouterOutlet } from '@angular/router';


@Component({
  selector: 'app-product-demande',
  templateUrl: './product-demande.component.html',
  styleUrl: './product-demande.component.scss',
  standalone:true,
  imports: [RouterOutlet, ProductDemandeHeuresComponent,ProductAdressComponent, MatFormFieldModule, MatInputModule, MatDatepickerModule, MatCardModule, DatePipe],

  providers: [provideNativeDateAdapter()],
  changeDetection: ChangeDetectionStrategy.OnPush

})
export class ProductDemandeComponent {

  selected = model<Date | null>(null);

  constructor(private router : Router){}

  suivant(){
   
   if(this.router.url == "/products/adress"){
    this.router.navigate(['/products/heures']);
   }

   if(this.router.url == "/products/heures"){
    this.router.navigate(['/products/telephone']);
   }

   if(this.router.url == "/products/telephone"){
    this.router.navigate(['/products/photo']);
   }

   

    
  }

  retour(){
    if(this.router.url == "/products/heures"){
      this.router.navigate(['/products/adress']);
     }

     if(this.router.url == "/products/telephone"){
      this.router.navigate(['/products/heures']);
     }

     if(this.router.url == "/products/photo"){
      this.router.navigate(['/products/telephone']);
     }
  }

}
