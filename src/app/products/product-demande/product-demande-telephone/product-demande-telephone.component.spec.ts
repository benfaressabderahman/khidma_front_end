import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDemandeTelephoneComponent } from './product-demande-telephone.component';

describe('ProductDemandeTelephoneComponent', () => {
  let component: ProductDemandeTelephoneComponent;
  let fixture: ComponentFixture<ProductDemandeTelephoneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductDemandeTelephoneComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ProductDemandeTelephoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
