import { ChangeDetectionStrategy, Component } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

@Component({
  selector: 'app-product-demande-telephone',
  standalone: true,
  imports: [MatFormFieldModule, MatInputModule, MatSelectModule],
  templateUrl: './product-demande-telephone.component.html',
  styleUrl: './product-demande-telephone.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductDemandeTelephoneComponent {

}
