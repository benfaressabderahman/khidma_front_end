import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDemandeComponent } from './product-demande.component';

describe('ProductDemandeComponent', () => {
  let component: ProductDemandeComponent;
  let fixture: ComponentFixture<ProductDemandeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductDemandeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ProductDemandeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
