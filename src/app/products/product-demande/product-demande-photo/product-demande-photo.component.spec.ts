import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDemandePhotoComponent } from './product-demande-photo.component';

describe('ProductDemandePhotoComponent', () => {
  let component: ProductDemandePhotoComponent;
  let fixture: ComponentFixture<ProductDemandePhotoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductDemandePhotoComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ProductDemandePhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
