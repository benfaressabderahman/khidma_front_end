import { Component } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';

@Component({
  selector: 'app-product-demande-photo',
  standalone: true,
  imports: [MatIconModule],
  templateUrl: './product-demande-photo.component.html',
  styleUrl: './product-demande-photo.component.scss'
})
export class ProductDemandePhotoComponent {

}
