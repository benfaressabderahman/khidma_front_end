import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../../_services/shared.service';
import { NgxsOnInit } from '@ngxs/store';

@Component({
  selector: 'app-product-demande-heures',
  standalone: true,
  templateUrl: './product-demande-heures.component.html',
  styleUrl: './product-demande-heures.component.scss'
})
export class ProductDemandeHeuresComponent implements OnInit{

   heures  = ['07:30', '08:30', '09:30', '10:30', '11:30', '12:30', '13:30', '14:30', '15:30', '16:30', '17:30', '18:30', '19:30', '20:30', '21:30', '22:30']
   c :any = 'string';

   constructor(private sharedSerivce : SharedService){}

   ngOnInit(): void {

   }



   getHeure(heure : any){
    this.sharedSerivce.selectHeures(heure);
   }

}
