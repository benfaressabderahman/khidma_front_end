import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDemandeHeuresComponent } from './product-demande-heures.component';

describe('ProductDemandeHeuresComponent', () => {
  let component: ProductDemandeHeuresComponent;
  let fixture: ComponentFixture<ProductDemandeHeuresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductDemandeHeuresComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ProductDemandeHeuresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
