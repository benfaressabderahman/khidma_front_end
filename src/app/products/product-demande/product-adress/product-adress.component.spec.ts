import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductAdressComponent } from './product-adress.component';

describe('ProductAdressComponent', () => {
  let component: ProductAdressComponent;
  let fixture: ComponentFixture<ProductAdressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductAdressComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ProductAdressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
