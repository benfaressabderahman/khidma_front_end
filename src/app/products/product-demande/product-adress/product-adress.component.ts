import { ChangeDetectionStrategy, Component } from '@angular/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { SharedService } from '../../../_services/shared.service';

@Component({
  selector: 'app-product-adress',
  standalone:true,
  imports: [MatFormFieldModule, MatInputModule, MatIconModule],
  templateUrl: './product-adress.component.html',
  styleUrl: './product-adress.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,

})
export class ProductAdressComponent {

  constructor(private sharedSerivce : SharedService){}
  getdata(){
    this.sharedSerivce.currentData.subscribe(
      (data) => {
      }
    )
   }
}
