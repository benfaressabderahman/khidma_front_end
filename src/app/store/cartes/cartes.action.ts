import { Cartes } from "./cartes.model";

export class AddCartes{
    static readonly type= '[Post page] AddPost';
    constructor(public payload: Cartes){}
}


export class RemoveCartes{
    static readonly type= '[Post page] RemovePost';
    constructor(public id: string){}
}