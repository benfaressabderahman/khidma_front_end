export interface Cartes {
    name: string,
    img: String,
    navigation: String;
}

export class CartesStateModel {
    public listCartes!: Cartes;
}