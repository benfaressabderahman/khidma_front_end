import { Injectable } from '@angular/core';
import { State, Action, StateContext, Selector } from "@ngxs/store";
import { CartesStateModel, cartes } from './cartes.model';
import { AddCartes, RemoveCartes } from './cartes.action';

//define the state
@State<CartesStateModel>({
    name: 'cartess',
    defaults: {
        listCartes: {
            name: '',
            img: ''
        }
    }
})

//add this this to the app.config.ts provider -- can create more than one state
@Injectable()
export class CartesState{
    constructor(){}

    @Selector()
    static cartess(state : cartes){
        return state;
    }

    //create the post action
    @Action(AddCartes) addPost(ctx: StateContext<CartesStateModel>, action:cartes){
        const state = ctx.getState();
        ctx.setState({
            ...state,
            listCartes: action
        })

    }

    //remove the post action
    @Action(RemoveCartes) removePost(ctx: StateContext<CartesStateModel>, action:cartes){
        const state = ctx.getState();
        ctx.patchState({
            listCartes:action
        })
    }

}