import { Injectable } from '@angular/core';
import { State, Action, StateContext } from "@ngxs/store";
import { PostsStateModel } from './products.model';
import { AddPost, RemovePost } from './products.action';

//define the state
@State<PostsStateModel>({
    name: 'posts',
    defaults: {
        listPosts: []
    }
})

//add this this to the app.config.ts provider -- can create more than one state
@Injectable()
export class PostState{
    constructor(){}

    //create the post action
    @Action(AddPost) addPost(ctx: StateContext<PostsStateModel>, action:AddPost){
        const state = ctx.getState();
        ctx.setState({
            ...state,
            listPosts: [
                ...state.listPosts,
                action.payload
            ]
        })

    }

    //remove the post action
/*     @Action(RemovePost) removePost(ctx: StateContext<PostsStateModel>, action:RemovePost){
        const state = ctx.getState();
        ctx.patchState({
            listPosts:[
                ...state.listPosts.filter(post => post.firstName !== action)
            ]
        })
    } */

}