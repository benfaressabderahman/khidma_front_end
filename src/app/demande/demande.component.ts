import { AfterViewInit, Component, ComponentFactoryResolver, ElementRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MatStepperModule} from '@angular/material/stepper';
import { DemandeMenageComponent } from './demande-menage/demande-menage.component';
import { DemandeJardinageComponent } from './demande-jardinage/demande-jardinage.component';
import { DemandeInformatiqueComponent } from './demande-informatique/demande-informatique.component';
import { DemandeEnfantsComponent } from './demande-enfants/demande-enfants.component';
import { DemandeDemenagementComponent } from './demande-demenagement/demande-demenagement.component';
import { DemandeBricolageComponent } from './demande-bricolage/demande-bricolage.component';
import { DemandeAnimauxComponent } from './demande-animaux/demande-animaux.component';
import { DemandeAideDomicileComponent } from './demande-aide-domicile/demande-aide-domicile.component';
import { DemandeAideCoursParticulierComponent } from './demande-aide-cours-particulier/demande-aide-cours-particulier.component';
import { SharedService } from '../_services/shared.service';
import { Demande } from '../madels/Demande';
import { ProductDemandeHeuresComponent } from '../products/product-demande/product-demande-heures/product-demande-heures.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../config/dialog/dialog.component';
import { ProductDemandeComponent } from '../products/product-demande/product-demande.component';

// const components = [{'steps': 3, Adress}];



@Component({
  selector: 'app-demande',
  standalone:true,
  imports: [
    CommonModule,
    MatButtonModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    DemandeMenageComponent,
    DemandeJardinageComponent,
    DemandeInformatiqueComponent,
    DemandeEnfantsComponent, DemandeDemenagementComponent, DemandeBricolageComponent, DemandeAnimauxComponent, DemandeAideDomicileComponent,
    DemandeAideCoursParticulierComponent,
    ProductDemandeComponent
    
  ],
  templateUrl: './demande.component.html',
  styleUrl: './demande.component.scss'
})
export class DemandeComponent  implements OnInit, AfterViewInit {
  currentStep = 1;
  firstFormGroup!: FormGroup;
  secondFormGroup!: FormGroup;
  category : any;
  frn !:Demande;
  namedemane = 'montageMeublesCuisine';
  step : number = 1;

  currentSteps = 0;
  steps = ['Step 1', 'Step 2', 'Step 3'];
stepss !: any;
  thirdFormGroup!: FormGroup;


  name : String = 'salut';
  animal : String = 'KANIBAL';

  constructor(private _formBuilder: FormBuilder,
    private sharedService : SharedService,
    private componentFactoryResolver: ComponentFactoryResolver  ) {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({
      thirdCtrl: ['', Validators.required]
    });
  }

  ngOnInit() {

    this.sharedService.steps.subscribe(
      (datas)=>{
        this.stepss = datas;

        this.sharedService.demandess.filter(
          (data : any) => {
            if(data.nameDemande === this.stepss.name){
              this.frn = data;
            }
          }
        );
      }
    )




    this.loadPersonComponent(this.frn, this.step);

    var a='a';
    this.sharedService.category.subscribe(
      (data) => {
      //  this.category= JSON.parse(data);
     //   console.log(data);
      }
    )
    this.firstFormGroup = this._formBuilder.group({
      firstName: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      address: ['', Validators.required]
    });
  }

  ngAfterViewInit(): void {
    this.loadPersonComponent(this.frn, 1);
  }
  
  @ViewChild('dynamicContainer', { read: ViewContainerRef, static: true }) container!: ViewContainerRef;



  loadPersonComponent(b : Demande, step : number) {
    for(let caze of b.etapes.nomsEtape){
      if(caze.step === step){
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(caze.nameD);
        var w = this.container;
        this.container.createComponent(componentFactory);
      }
    }
  }

  nextStep() {
    this.loadPersonComponent(this.frn, this.frn.etapes.nbEtape+1);
    if (this.currentStep === 1 && this.firstFormGroup.valid) {
      this.currentStep++;
    } else if (this.currentStep === 2 && this.secondFormGroup.valid) {
      this.currentStep++;
    }
  }

  previousStep() {
    this.currentStep--;
  }

  goToStep(step: number) {
    this.currentStep = step;
  }

  reset() {
    this.currentStep = 1;
    this.firstFormGroup.reset();
    this.secondFormGroup.reset();
  }

  retourStep(){
    this.container.clear();
    this.step --;
    this.loadPersonComponent(this.frn, this.step);
  }

  nextSteps(){
    this.container.clear();
    this.step ++;
    this.loadPersonComponent(this.frn, this.step);
  }

  isStepValid(stepIndex: number): boolean {
    switch (stepIndex) {
      case 0:
        return this.firstFormGroup.valid;
      case 1:
        return this.secondFormGroup.valid;
      case 2:
        return this.thirdFormGroup.valid;
      default:
        return false;
    }
  }


} 