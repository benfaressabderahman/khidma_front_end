import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeMenageComponent } from './demande-menage.component';

describe('DemandeMenageComponent', () => {
  let component: DemandeMenageComponent;
  let fixture: ComponentFixture<DemandeMenageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandeMenageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DemandeMenageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
