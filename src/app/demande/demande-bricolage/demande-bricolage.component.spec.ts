import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeBricolageComponent } from './demande-bricolage.component';

describe('DemandeBricolageComponent', () => {
  let component: DemandeBricolageComponent;
  let fixture: ComponentFixture<DemandeBricolageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandeBricolageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DemandeBricolageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
