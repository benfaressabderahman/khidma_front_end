import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeJardinageComponent } from './demande-jardinage.component';

describe('DemandeJardinageComponent', () => {
  let component: DemandeJardinageComponent;
  let fixture: ComponentFixture<DemandeJardinageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandeJardinageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DemandeJardinageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
