import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeInformatiqueComponent } from './demande-informatique.component';

describe('DemandeInformatiqueComponent', () => {
  let component: DemandeInformatiqueComponent;
  let fixture: ComponentFixture<DemandeInformatiqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandeInformatiqueComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DemandeInformatiqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
