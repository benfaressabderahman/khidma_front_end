import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeAideCoursParticulierComponent } from './demande-aide-cours-particulier.component';

describe('DemandeAideCoursParticulierComponent', () => {
  let component: DemandeAideCoursParticulierComponent;
  let fixture: ComponentFixture<DemandeAideCoursParticulierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandeAideCoursParticulierComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DemandeAideCoursParticulierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
