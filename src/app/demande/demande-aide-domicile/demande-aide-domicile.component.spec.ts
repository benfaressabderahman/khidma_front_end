import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeAideDomicileComponent } from './demande-aide-domicile.component';

describe('DemandeAideDomicileComponent', () => {
  let component: DemandeAideDomicileComponent;
  let fixture: ComponentFixture<DemandeAideDomicileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandeAideDomicileComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DemandeAideDomicileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
