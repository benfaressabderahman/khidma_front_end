import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeAnimauxComponent } from './demande-animaux.component';

describe('DemandeAnimauxComponent', () => {
  let component: DemandeAnimauxComponent;
  let fixture: ComponentFixture<DemandeAnimauxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandeAnimauxComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DemandeAnimauxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
