import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeEnfantsComponent } from './demande-enfants.component';

describe('DemandeEnfantsComponent', () => {
  let component: DemandeEnfantsComponent;
  let fixture: ComponentFixture<DemandeEnfantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandeEnfantsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DemandeEnfantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
