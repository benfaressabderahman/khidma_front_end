import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandeDemenagementComponent } from './demande-demenagement.component';

describe('DemandeDemenagementComponent', () => {
  let component: DemandeDemenagementComponent;
  let fixture: ComponentFixture<DemandeDemenagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DemandeDemenagementComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(DemandeDemenagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
