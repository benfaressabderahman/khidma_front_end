import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ProductAdressComponent } from '../products/product-demande/product-adress/product-adress.component';
import { ProductDemandeHeuresComponent } from '../products/product-demande/product-demande-heures/product-demande-heures.component';
import { ProductDemandeTelephoneComponent } from '../products/product-demande/product-demande-telephone/product-demande-telephone.component';

@Injectable({
  providedIn: 'root'
})
export class SharedService {






  demandess = 
  [{
    category: 'bricolage',
    sousCategory : 'ameublement',
    nameDemande : 'Français',
    etapes : {nbEtape: 3,
    nomsEtape:[{step: 1 ,nameD : ProductAdressComponent},{step: 2 ,nameD: ProductAdressComponent} , {step: 3 ,nameD : ProductAdressComponent} ]}
  },
  {
    category: 'bricolage',
    sousCategory : 'ameublement',
    nameDemande : 'assemblageMeubles',
    etapes : {nbEtape: 3,
    nomsEtape:[{step: 1 ,nameD : ProductAdressComponent},{step: 2 ,nameD: ProductAdressComponent} , {step: 3 ,nameD : ProductAdressComponent} ]}
  },
  {
    category: 'bricolage',
    sousCategory : 'ameublement',
    nameDemande : 'Français',
    etapes : {nbEtape: 3,
    nomsEtape:[{step: 1 ,nameD : ProductAdressComponent},{step: 2 ,nameD: ProductAdressComponent} , {step: 3 ,nameD : ProductAdressComponent} ]}
  },
  {
    category: 'bricolage',
    sousCategory : 'ameublement',
    nameDemande : 'montageMeubles',
    etapes : {nbEtape: 3,
    nomsEtape:[{step: 1 ,nameD : ProductAdressComponent},{step: 2 ,nameD: ProductAdressComponent} , {step: 3 ,nameD : ProductAdressComponent} ]}
  },
  {
    category: 'bricolage',
    sousCategory : 'ameublement',
    nameDemande : 'montageLit',
    etapes : {nbEtape: 3,
    nomsEtape:[{step: 1 ,nameD : ProductAdressComponent},{step: 2 ,nameD: ProductAdressComponent} , {step: 3 ,nameD : ProductAdressComponent} ]}
  },
  {
    category: 'bricolage',
    sousCategory : 'ameublement',
    nameDemande : 'montageMeublesCuisine',
    etapes : {nbEtape: 3,
    nomsEtape:[{step: 1 ,nameD : ProductAdressComponent},{step: 2 ,nameD: ProductDemandeHeuresComponent} , {step: 3 ,nameD : ProductDemandeTelephoneComponent} ]}
  }
  
]





categories = [
  {
    id : 1,
    category: 'bricolage',
    icon: 'build',
    sous_categorie: [
      {
        id: 1,
        name : 'pose et fixation'
      },
      {
        id: 2,
        name : 'Ameublement'
      },
      {
        id: 3,
        name : 'renovation des murs'
      },
      {
        id: 4,
        name : 'renovation de sols'
      },
      {
        id: 5,
        name : 'ectromenager'
      },
      {
        id: 6,
        name : 'electricité'
      },
      {
        id: 7,
        name : 'plomberie'
      },
      {
        id: 8,
        name : 'rrurerie'
      },
      {
        id: 9,
        name : 'Isolation'
      },
      {
        id: 10,
        name : 'Réparation'
      }
    ]
  },
  {
    id : 2,
    category: 'Cours particulier',
    icon : 'school',
    sous_categorie: [
      {
        id: 1,
        name : 'Langues',
        sous_sous_category:[
          {
            id: 1,
            name : 'Français'
          },{
            id: 2,
            name : 'Anglais'
          },{
            id: 3,
            name : 'Espagnol'
          },{
            id: 4,
            name : 'Allemand'
          },{
            id: 5,
            name : 'Italien'
          }
        ]
      },
      {
        id: 2,
        name : 'Matières',
        sous_sous_category:[
          {
            id: 1,
            name : 'Mathématiques'
          },{
            id: 2,
            name : 'Histoire'
          },{
            id: 3,
            name : 'Géographie'
          },{
            id: 4,
            name : 'Philosophie'
          },{
            id: 5,
            name : 'SVT'
          },{
            id: 6,
            name : 'Physique'
          },{
            id: 7,
            name : 'Chimie'
          }
        ]
      }
    ]
  },
  {
    id : 3,
    category: 'Enfants',
    icon : 'child_friendly',
    sous_categorie: [
      {
        id: 1,
        name : 'Garde ponctuelle'
      },
      {
        id: 1,
        name : 'Garde week-end, vacances'
      },
      {
        id: 1,
        name : 'Garde périscolaire'
      },
      {
        id: 1,
        name : 'Garde longue durée'
      }
    ]
  }
  ,  {
    id : 4,
    category: 'Ménage',
    icon : 'cleaning_services',
    sous_categorie: [
      {
        id: 1,
        name : 'Ménage',
        sous_sous_category:[
          {
            id: 1,
            name : 'Ménage à domicile'
          },{
            id: 2,
            name : 'Ménage de printemps'
          },{
            id: 3,
            name : 'Ménage état des lieux'
          },{
            id: 4,
            name : 'Ménage de location saisonnière'
          },{
            id: 5,
            name : 'Ménage après un événement'
          },{
            id: 6,
            name : 'Repassage'
          }, {
            id: 7,
            name : 'Nettoyage de vitres'
          },{
            id: 8,
            name : 'Lessiver un mur'
          },{
            id: 9,
            name : 'Aide au rangement de la maison'
          },{
            id: 10,
            name : 'Nettoyage de fin de chantier'
          },{
            id: 11,
            name : 'Nettoyage de logement insalubre'
          }
        ]
      },
      {
        id: 2,
        name : 'Nettoyage électroménager',
        sous_sous_category: [
          {
            id: 8,
            name : 'Nettoyage de hotte'
          },{
            id: 9,
            name : 'Nettoyage de frigo'
          },{
            id: 10,
            name : 'Nettoyage de four'
          },{
            id: 11,
            name : 'Nettoyage de congélateur'
          }
        ]
      },
      {
        id: 3,
        name : 'Nettoyage textile',
        sous_sous_category: [
          {
            id: 8,
            name : 'Nettoyage de canapé'
          },{
            id: 9,
            name : 'Nettoyage de tapis'
          },{
            id: 10,
            name : 'Nettoyage de moquette'
          },{
            id: 11,
            name : 'Nettoyage de matelas'
          },{
            id: 11,
            name : 'Nettoyage de rideaux'
          }
        ]
      },
      {
        id: 4,
        name : 'Nettoyage voiture',
        sous_sous_category: [
          {
            id: 8,
            name : 'Lavage automobile'
          },{
            id: 9,
            name : 'Nettoyage intérieur de voiture'
          },{
            id: 10,
            name : 'Nettoyage extérieur de voiture'
          },{
            id: 11,
            name : 'Lustrage de voiture'
          }
        ]
      }
    ]
  },
  {
    id : 5,
    category: 'Démenagement',
    icon : 'moving'
  },
  {
    id : 6,
    category: 'Jardin',
    icon : 'grass'
  },
  {
    id : 7,
    category: 'Animaux',
    icon : 'pets',
    sous_categorie: [
      {
        id: 8,
        name : 'Garde de chien'
      },{
        id: 9,
        name : 'Garde de chat'
      },{
        id: 10,
        name : 'Faire promener son chien'
      },{
        id: 11,
        name : 'Garde autres animaux'
      }
    ]
  }
]


































  private adressSource = new BehaviorSubject<string>('Default Data');
  adress = this.adressSource.asObservable();

  private dataSource = new BehaviorSubject<string>('Default Data');
  currentData = this.dataSource.asObservable();

  private categorySource = new BehaviorSubject<string>('Default Data');
  category = this.dataSource.asObservable();

  private stepsSource = new BehaviorSubject<string>('Default Data');
  steps = this.dataSource.asObservable();

  selectHeures(data: string) {
    this.dataSource.next(data);
  }


  
  selectAdress(data: string) {
    this.adressSource.next(data);
  }

  selectCategory(data: string) {
    this.dataSource.next(data);
  }

  selectSteps(data: string) {
    this.dataSource.next(data);
  }

  demandes(){
    return this.demandess;
  }


  private sousCategorySource = new BehaviorSubject<string>('Default Data');
  sousCategory = this.dataSource.asObservable();
  selectSousCategory(sousCategory : any){
    this.dataSource.next(sousCategory);

  }

}
