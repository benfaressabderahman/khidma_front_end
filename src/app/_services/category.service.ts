import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


const API_CATEGORY = 'http://localhost:8080/api/category/';

const API_SOUS_CATEGORY = 'http://localhost:8080/api/sousCategory/';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private http : HttpClient) { }


  getCategories(): Observable<any>{
    return this.http.get(API_CATEGORY + 'getCategories', { responseType: 'text' } )
  }

  postCategories(name:String): Observable<any>{
    return this.http.post(API_CATEGORY + 'postCategory', name, { responseType: 'text' } )
  }

  deleteCategories(id:number): Observable<any>{
    return this.http.delete(API_CATEGORY + 'deleteCategory' )
  }

  getSousCategory(): Observable<any>{
    return this.http.get(API_SOUS_CATEGORY + "get", { responseType: 'text' })
  }

  getSousCategoryById(id:number): Observable<any>{
    return this.http.get(API_SOUS_CATEGORY + "getById/" + id, { responseType: 'text' })
  }

  postSousCategory(name:String, idCategory: String): Observable<any>{
    return this.http.post(API_SOUS_CATEGORY + "posts", {name, idCategory}, { responseType: 'text' })
  }




}
