import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { post } from "../store/products/products.model";
import { StorageService } from "./storage.service";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const API_PRODUCTS = 'http://localhost:8080/api/Products/';

const API_LIKES = 'http://localhost:8080/api/likes/';

const httpOptions_image = {
  headers: new HttpHeaders({ 'enctype': 'multipart/form-data'  })
};

@Injectable({
    providedIn: 'root',
  })
  export class ProductsService {

    constructor(private http: HttpClient, private storageService : StorageService) {}

  postProducts(formData : FormData): Observable<any> {
   const userId = String(this.storageService.idSessionUser());
   if(userId){
   formData.append("userId", userId)}
   return this.http.post( API_PRODUCTS + "post", formData,  httpOptions_image);
  }

  getProductById(id : String): Observable<any> {
    return this.http.get(API_PRODUCTS + "getById/"+ id , { responseType: 'text' });
}
  
    updateProduct(product: any): Observable<any> {
      return this.http.put(API_PRODUCTS, product);
    }
    
    deleteProduct(id: any): Observable<any> {
      return this.http.delete(API_PRODUCTS + id);
    }
  
    getProducts(): Observable<any> {
      return this.http.get(API_PRODUCTS + "get" , { responseType: 'text' });
  }


  gettnote(rating:any) : Observable<any> {
    return this.http.get('http://localhost:8080/api/getNote', rating);
  }



  addLike(idDemande : number, userId : number): Observable<any> {
    return this.http.post(
      API_LIKES + "add",
         {
          idDemande, userId
         }
         ,
         httpOptions
 
       );
  }

  getProductsBySousCategory(idSousCategory: any){
    return this.http.get(API_PRODUCTS+ "getBySousCategory/"+idSousCategory, { responseType: 'text' })
  }
}