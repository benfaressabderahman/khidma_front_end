import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:8080/api/test/';

const API_URL_USER = 'http://localhost:8080/api/users/';


const API_USER_IMAGE_URL = 'http://localhost:8080/api/users/';

const httpOptions_image = {
  headers: new HttpHeaders({ 'enctype': 'multipart/form-data'  })
};

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }

  findUserById(): Observable<any> {
    return this.http.get(API_URL_USER + 'user', { responseType: 'text' });
  }
  
  getModeratorBoard(): Observable<any> {
    return this.http.get(API_URL + 'mod', { responseType: 'text' });
  }

  getAdminBoard(): Observable<any> {
    return this.http.get(API_URL + 'admin', { responseType: 'text' });
  }

  postImageUser(formData : FormData): Observable<any>{
    return this.http.post(API_USER_IMAGE_URL + 'upload', formData, httpOptions_image)
  }

  getImageUser(id : number) : Observable<any>{
    return this.http.get(API_USER_IMAGE_URL+'download/' + id,  { responseType: 'blob' });
  }

  deletePhotoUser(id:number) : Observable<any>{
    return this.http.delete(API_USER_IMAGE_URL+'delete/'+id);
  }
}
