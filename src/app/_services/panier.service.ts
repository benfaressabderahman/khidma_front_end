import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const API_PANIER = 'http://localhost:8080/api/panier/';


@Injectable({
  providedIn: 'root'
})

export class PanierService {


  constructor(private http : HttpClient) { }


  adPanier(product : any){
    return this.http.post( API_PANIER + "add", product)
  }
}
