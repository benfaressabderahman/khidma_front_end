import { Component, OnInit } from '@angular/core';
import { StorageService } from '../_services/storage.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from '../_services/user.service';
import { DomSanitizer } from '@angular/platform-browser';
import { CalculeService } from '../_services/calcule.service';
import { CategoryService } from '../_services/category.service';

@Component({
  selector: 'app-profile',
  standalone: true,
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  currentUser: any;
  selectedFile!: File | null;
  imageUrl: any;
  dataGetImage: any;

  constructor(
    private userService: UserService,
    private storageService: StorageService,
    private http: HttpClient,
    private calculService: CalculeService,
    private categorieService : CategoryService
  ) {}

  ngOnInit(): void {
    this.currentUser = this.storageService.getUser();
 /*    this.userService. */


    this.userService.getImageUser(this.currentUser.id).subscribe({
      next: (response:any) => {
        this.calculService
          .blobToBase64(response)
          .then((base64) => {
            this.dataGetImage = base64;
          })
          .catch((err) => {
            console.error('Error converting blob to base64', err);
          });
      },
    });
  }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
    if (this.selectedFile) {
      const formData = new FormData();
      formData.append('image', this.selectedFile);
      formData.append('userId', this.currentUser.id);
      this.userService.postImageUser(formData).subscribe({
        next: (data) => {
          this.selectedFile = data;
        },
      });
    }
  }

  uploadPhoto(e: any) {
    if (this.selectedFile) {
      const formData = new FormData();
      formData.append('file', this.selectedFile);
    }
  }

  supprPhoto(currentUser:any){
    this.userService.deletePhotoUser(currentUser.id).subscribe({
      next : (data: any) =>{
        const a  = data
      }
    })
  }
}
