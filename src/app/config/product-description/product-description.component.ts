import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { ProductsService } from '../../_services/products.service';
import { Product } from '../../madels/product.model';
import { PanierService } from '../../_services/panier.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-product-description',
  standalone: true,
  templateUrl: './product-description.component.html',
  styleUrl: './product-description.component.scss'
})
export class ProductDescriptionComponent implements OnInit{

   allHoverImages:any ;
   
   idDemande !:String | null ;

   currentImage : any;

   productImage:any;
/*    = document.querySelectorAll('.hover-container div img');
 */   imgContainer:any;
   product !: Product;
/*    = document.querySelector('.img-container');
 */  

constructor(private activatedRoute: ActivatedRoute,
   private productsService : ProductsService,
    private panierService : PanierService,
    private http: HttpClient
){}

  ngOnInit(): void {
      
    this.idDemande = this.activatedRoute.snapshot.paramMap.get('idDemande');


    if(this.idDemande){
    this.productsService.getProductById(this.idDemande).subscribe(
        data =>{
                this.product = JSON.parse(data);
                this.currentImage = this.product.productImage[0].data;
        }
    )}

    

  
/*   window.addEventListener('DOMContentLoaded', () => {
      this.allHoverImages[0].parentElement.classList.add('active');
  });
  
  this.allHoverImages.forEach((image:any) => {
      image.addEventListener('mouseover', () =>{
          this.imgContainer.querySelector('img').src = image.src;
          this.resetActiveImg();
          image.parentElement.classList.add('active');
      });
  }); */
}
/*    resetActiveImg(){
      this.allHoverImages.forEach((img:any) => {
          img.parentElement.classList.remove('active');
      });
  } */

  imag(products:any){
    return 'data:application/json;base64,' + products;
    }

    changeImage(image : any){
        this.currentImage = image;
    }

/*     like(idDemande: any) {
        this.productsService
          .addLike(idDemande, this.product.userId)
          .subscribe({
            next: (data) => {},
          });
      }  */

      addPanier(product : any){
        this.panierService.adPanier(product).subscribe(
          (data)=> {

          }
        )
      }

      getPanier(product : Product){
        this.http.get('http://localhost:8080/api/panier/get/' + product.userId,  { responseType: 'text' }).subscribe(
          (data) => {
            
          }
        )

      }
}