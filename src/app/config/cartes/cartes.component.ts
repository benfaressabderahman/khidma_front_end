import { Component, inject } from '@angular/core';
import { CarteService } from './carte.service';
import { Observable } from 'rxjs';
import { Cartes } from '../../store/cartes/cartes.model';
import { Store } from '@ngxs/store';
import { AddCartes } from '../../store/cartes/cartes.action';
import { Route, Router } from '@angular/router';


@Component({
  selector: 'app-cartes',
  standalone: true,
  templateUrl: './cartes.component.html',
  styleUrl: './cartes.component.scss'
})
export class CartesComponent {

  public store = inject(Store);

  public ecarte$: Observable<Cartes> = this.store.select(state => state.cartes.listCartes);

carte : any;
name?:any ;
img?:any;
nav ?: Cartes ;
constructor(private carteService : CarteService, private route : Router){}

ngOnInit(){
  this.carte = this.carteService.getCarte();
}

state(e : any) : void {

  this.name  = e.srcElement.alt;
  this.nav = this.carte.find((item: { name: any; }) => item.name.includes( this.name));


  const carte: any = {
    name: this.name,
    img: this.img,
  }
  var a = this.store.dispatch(new AddCartes(carte));


 this.nav = this.carte.find((item: { name: any; }) => item.name.includes( this.name));

 this.route.navigate([this.nav?.navigation]);
}

c():void{
}

}
