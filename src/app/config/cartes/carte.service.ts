import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarteService {

private cartes = [    
    {"id": 1, "name": "Ménage", "img": "https://picsum.photos/id/1/900/500", "navigation":"/products/menage" },
    {"id": 2,"name": "Jardin", "img": "https://picsum.photos/id/2/900/500", "navigation":"/products/jardin"},
    {"id": 3,"name": "Chantier", "img": "https://picsum.photos/id/3/900/500", "navigation":"/products/chantier"}
]

  private users: string[] = ['John', 'Jane', 'Doe'];

  getUsers(): string[] {
    return this.users;
  }

  addUser(user: string): void {
    this.users.push(user);
  }

  getCarte(): any{
   return this.cartes;
  }
}

