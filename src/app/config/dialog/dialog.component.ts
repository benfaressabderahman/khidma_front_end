import { Component, EventEmitter, Inject, Input, OnInit, Output, ViewChild, ViewContainerRef } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import {MAT_DIALOG_DATA, MatDialogActions, MatDialogClose, MatDialogContent, MatDialogModule, MatDialogRef, MatDialogTitle} from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { SharedService } from '../../_services/shared.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-dialog',
  standalone:true,
  imports: [MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatDialogClose,
    ReactiveFormsModule,
    MatIconModule,
    CommonModule],
  templateUrl: './dialog.component.html',
  styleUrl: './dialog.component.scss'
})
export class DialogComponent implements OnInit{

data: any = null;  // The '!' is a non-null assertion operator
SousSousCtegory : any;

  constructor(
    public sharedService : SharedService,
    private route : Router
  ){

    }
    ngOnInit(){
      this.sharedService.sousCategory.subscribe(
        (data) => {
          this.data = data;
          for(let c of this.data.sous_categorie){
          console.log(c.name)
        }}
      )
    }

    // @ViewChild('dialog', { read: ViewContainerRef, static: true }) container!: ViewContainerRef;

    // onNoClick(): void {
    //   this.dialogRef.close();
    // }

    @Output() firstEvent = new EventEmitter<string>();

    @Output() secondEvent = new EventEmitter<string>();



    sendMessage() {
      this.firstEvent.emit('Hello from Child');
      this.SousSousCtegory = null;
      this.data = null;
    }

    goToSousSousCtegory(c: any){
      if(c.sous_sous_category){
        this.SousSousCtegory = c.sous_sous_category
      }
    }

    goToSousSteps(a : any){
      this.secondEvent.emit(a);
      this.sharedService.selectCategory(a);
      // this.route.navigate(['/demande']);
    }
}
