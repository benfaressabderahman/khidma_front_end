import { Component, ElementRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { StorageService } from '../_services/storage.service';
import { AuthService } from '../_services/auth.service';
import { EventBusService } from '../_shared/event-bus.service';
import { CategoryService } from '../_services/category.service';
import { Route, Router, RouterLink } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import { CommonModule } from '@angular/common';
import { SharedService } from '../_services/shared.service';
import { DialogComponent } from '../config/dialog/dialog.component';

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports:[CommonModule, MatIconModule, RouterLink, MatIconModule, DialogComponent],
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss'
})
export class NavbarComponent {

  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username?: string;

  eventBusSub?: Subscription;
  hiden:String = "hidden";

  categories!:any;

  sousCategories !: any;
  categoriess!:any;
  data : any;
  @ViewChild('dialog', { static: true }) dialogg!: ElementRef<HTMLDialogElement>;


  constructor(private storageService: StorageService,
    private authService: AuthService,
    private eventBusService: EventBusService,
    private categoryService : CategoryService,
    private route : Router,
    public sharedService : SharedService
  ) { }

    ngOnInit(){
      
    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

      this.username = user.username;
    }

    this.eventBusSub = this.eventBusService.on('logout', () => {
      this.logout();
    });

    this.categoryService.getCategories().subscribe({
      next: (data) =>{
        this.categories = JSON.parse(data);
      }
    });

    this.sharedService.categories.filter(
      (data) => {
        if(data.category === this.categoriess){
        }
      }
    );

  }

  logout(): void {
    this.storageService.clean();
  }


  hidden(){
    this.hiden = "hidden";
  }

  visible(){
    this.hiden = "c-layer__content";
  }

  sousCategory(category:any){
    this.categoryService.getSousCategoryById(category.idCategory).subscribe({
      next: (data)=>{
        this.sousCategories = JSON.parse(data);
       } }
    )
  }

  getProductsBysSousCategory(sousCategory: any){
    this.sharedService.selectCategory(sousCategory.name);
     this.route.navigate(["/demande", sousCategory])

  }


  goToMenu(a  :any){

  } 


  openDialog(a : any): void {
    if (this.dialogg.nativeElement) {
      this.sharedService.selectSousCategory(a);
      this.dialogg.nativeElement.showModal();

  }}


  firstEvent($event: any) {
    if (this.dialogg.nativeElement) {
      this.dialogg.nativeElement.close();

  }  }

  secondEvent($event: any) {
    console.log($event)
    if (this.dialogg.nativeElement) {
      this.dialogg.nativeElement.close();
      this.route.navigate(['/demande']);

  }  }

}
