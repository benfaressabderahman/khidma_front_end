import { Component, OnInit, TemplateRef, inject } from '@angular/core';
import { UserService } from '../_services/user.service';
import { ProductsService } from '../_services/products.service';
import {
  NgbDatepickerModule,
  NgbOffcanvas,
  NgbRatingConfig,
  OffcanvasDismissReasons,
} from '@ng-bootstrap/ng-bootstrap';
import { StorageService } from '../_services/storage.service';
import { CategoryService } from '../_services/category.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { Product } from '../madels/product.model';

@Component({
  selector: 'app-home',
  standalone:true,
  imports:[MatPaginatorModule],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  content?: string;
  isLoggedIn = false;
  data !: Array<Product>;
  rating: number = 2;

  private offcanvasService = inject(NgbOffcanvas);
  closeResult = '';

  categories: any;

  images : any;
  image1 :any;
  image2:any;

  imagess:any;
  x = 0;
  sousCategory :any;
  idSousCategory !: any;

  length = 500;
  pageSize = 10;
  pageIndex = 0;
  pageSizeOptions = [5, 10, 25];
  showFirstLastButtons = true;

  constructor(
    private userService: UserService,
    private productsService: ProductsService,
    private storageService: StorageService,
    config: NgbRatingConfig,
    private categoryService: CategoryService,
    private route : Router,
    private activatedRoute: ActivatedRoute
  ) {
    config.max = 5;
  }

  ngOnInit(): void {
    this.isLoggedIn = this.storageService.isLoggedIn();

    this.getCategories();

    //this.getProducts();

    this.idSousCategory = this.activatedRoute.snapshot.paramMap.get('idSousCategory');
    if(!this.idSousCategory){
      this.getProducts();
    }

    if(this.idSousCategory){
      this.getProductsBySousCategory(this.idSousCategory);
    }


  }

  note(e: any) {
    this.productsService.gettnote(this.rating).subscribe({
      next: (data) => {},
    });
  }

  like(products: any) {
    this.productsService
      .addLike(products.idDemande, products.userId)
      .subscribe({
        next: (data) => {},
      });
  }

  open(content: TemplateRef<any>) {
    this.offcanvasService
      .open(content, { ariaLabelledBy: 'offcanvas-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    switch (reason) {
      case OffcanvasDismissReasons.ESC:
        return 'by pressing ESC';
      case OffcanvasDismissReasons.BACKDROP_CLICK:
        return 'by clicking on the backdrop';
      default:
        return `with: ${reason}`;
    }
  }

  mouseover(products :any){
    this.x=1;
  }

  mouseleave(products :any){
    this.x = 0;
  }

  getProducts(){
    this.productsService.getProducts().subscribe({
      next: (data) => {
        this.data = JSON.parse(data);
      },
      error: (err) => {
        if (err.error) {
          try {
            const res = JSON.parse(err.error);
           return res.message;
          } catch {
            return `Error with status: ${err.status} - ${err.statusText}`;
          }
        } else {
          return `Error with status: ${err.status}`;
        }
      },
    });
  }

  getCategories(){
    this.categoryService.getCategories().subscribe({
      next: (data) => {
        this.categories = JSON.parse(data);
        for (let category of this.categories) {
        }
      },
    });
  }

  imag(products:any){
  return 'data:application/json;base64,' + products.productImage[0].data;
  }

  getProductsBySousCategory(idSousCategory : number){

this.productsService.getProductsBySousCategory(idSousCategory).subscribe(
  data => {
    this.data = JSON.parse(data);
  }
)
  }

  handlePageEvent(event: PageEvent) {
    this.length = event.length;
    this.pageSize = event.pageSize;
    this.pageIndex = event.pageIndex;
  }

  selectProduct(product : Product){
    this.route.navigate(["/description", product]);

  }

}
