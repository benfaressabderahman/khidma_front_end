export class Product {
    userId !: String;
    productName !: String;
    productPrice !: String;
    productDescription !: String;
    idDemande !: number;
    productImage !: ProductImage[];
}

export class ProductImage {
    data !: String;
    idProductsImage !: number;
    name !: String;
}