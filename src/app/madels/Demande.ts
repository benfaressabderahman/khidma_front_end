import { Component, Type } from "@angular/core";

export class Demande{
    category !: String; 
    sousCategory !: String;
    nameDemande !: String;
    etapes !: Etapes;
}

export class Etapes{
    nbEtape !: number;
    nomsEtape !: NomsEtape[];
}

export class NomsEtape{
    step!: number;
    nameD !: Type<unknown>;
}