import { Component, OnInit } from '@angular/core';
/* import {
  GoogleLoginProvider,
  SocialAuthService,
} from '@abacritt/angularx-social-login'; */
import { FacebookLoginProvider } from '@abacritt/angularx-social-login';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './_services/storage.service';
import { AuthService } from './_services/auth.service';
import { EventBusService } from './_shared/event-bus.service';
import { Observable, Subscription } from 'rxjs';
import { CategoryService } from './_services/category.service';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserModule } from '@angular/platform-browser';
import { RouterOutlet } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProductDescriptionComponent } from './config/product-description/product-description.component';
import { ProductsComponent } from './products/products.component';
import { PluginManager } from '@ngxs/store/src/plugin-manager';

@Component({
    selector: 'app-root',
    standalone: true,
    templateUrl: './app.component.html',
    styleUrl: './app.component.scss',
    imports: [RouterOutlet, FormsModule,ReactiveFormsModule, NavbarComponent, ProductDescriptionComponent]
})
export class AppComponent implements OnInit {
  title = 'khidmati';
  user: any;
  loggedIn: any;
  private accessToken = '';

  formCategory = new FormGroup({
    name: new FormControl('')
  });

  formSousCategory = new FormGroup({
    name: new FormControl(''),
    selectedValue : new FormControl('')
  });

  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  showModeratorBoard = false;
  username?: string;

  eventBusSub?: Subscription;

  address =
    '3 Chem. de la Promenade des Messieurs, 77130 Montereau-Fault-Yonne'; // Votre adresse
  latitude!: number;
  longitude!: number;
  loading = true;
  error = false;
  lat = 51.678418;
  lng = 7.809007;
  zoom = 8;
  placeLat = 48.8588443; // Latitude du lieu
  placeLng = 2.2943506;
  categories: any;

/*   mapCenter!: google.maps.LatLngLiteral;
  markerPosition!: google.maps.LatLngLiteral; */

  constructor(
/*     private sauthService: SocialAuthService,
 */    private http: HttpClient,
    private storageService: StorageService,
    private authService: AuthService,
    private eventBusService: EventBusService,
    private categoryService: CategoryService
  ) {}

  ngOnInit() {
/*     this.sauthService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = user != null;
    }); */

    this.isLoggedIn = this.storageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.storageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
      this.showModeratorBoard = this.roles.includes('ROLE_MODERATOR');

      this.username = user.username;
    }

    this.eventBusSub = this.eventBusService.on('logout', () => {
      //this.logout();
    });

    /*     this.geocodeAddress('Votre adresse').subscribe(data => {
      if (data && data.results && data.results.length > 0) {
        const location = data.results[0].geometry.location;
        this.mapCenter = { lat: location.lat, lng: location.lng };
        this.markerPosition = { lat: location.lat, lng: location.lng };
      }
    }); */

    this.categoryService.getCategories().subscribe({
      next: (data) => {
        this.categories = JSON.parse(data);
      },
    });
  }

/*   signInWithFB(): void {
    this.sauthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }

  refreshToken(): void {
    this.sauthService.refreshAuthToken(GoogleLoginProvider.PROVIDER_ID);
  } */

  /*   logout(): void {
    this.authService.logout().subscribe({
      next: res => {
        this.storageService.clean();

        window.location.reload();
      },
      error: err => {
      }
    });
  } */

/*   getAccessToken(): void {
    this.sauthService
      .getAccessToken(GoogleLoginProvider.PROVIDER_ID)
      .then((accessToken) => (this.accessToken = accessToken));
  } */

  getGoogleCalendarData(): void {
    if (!this.accessToken) return;

    this.http
      .get('https://www.googleapis.com/calendar/v3/calendars/primary/events', {
        headers: { Authorization: `Bearer ${this.accessToken}` },
      })
      .subscribe((events) => {
        alert('Look at your console');
      });
  }

/*   signOut(): void {
    this.sauthService.signOut();
  } */

  /*   geocodeAddress(address: string): Observable<any> {
    const apiKey = 'VOTRE_CLÉ_API'; // Remplacez par votre clé d'API Google Maps
    const geocodingUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(address)}&key=AIzaSyB4hgHUPyngfQUVSlYrr9yzR1FD4MM8OVQ`;
    return this.http.get<any>(geocodingUrl);
  } */

  onSubmitCategory(): void {
    const { name } = this.formCategory.value;

    if (name) {
      this.categoryService.postCategories(name).subscribe({
        next: (data) => {},
      });
    }
  }

  onSubmitSousCategory(): void {
    const { name, selectedValue } = this.formSousCategory.value;

    if (name && selectedValue) {
      this.categoryService.postSousCategory(name, selectedValue).subscribe({
        next: (data) => {
        }
      });
    }
  }
}
